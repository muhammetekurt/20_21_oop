#include "LaserSensor.h"


/**
* Constructure of the LaserSensor
*/
LaserSensor::LaserSensor() {
	updateSensor(ranges);
}
/**
* This function returns distance information of the sensor with index.
* @param    int index
*/
float LaserSensor::getRange(int index) {
	return this->ranges[index];
}

/**
* This function, uploads the robot's current sensor distance values to the ranges array
* @param  float range array
*/

void LaserSensor::updateSensor(float ranges[]) {

	for (int i = 0; i < 181; i++)
	{
		this->ranges[i] = ranges[i];
	}
}
/**
* This function returns the minimum of the distance values
* @param    int index
*/
float LaserSensor::getMin(int& index) {
	updateSensor(ranges);
	float min;
	min = ranges[0];
	index = 0;
	for (int i = 0; i < 181; i++)
	{
		if (ranges[i] < min)
		{
			min = ranges[i];
			index = i;
		}
	}
	return min;
}
/**
* This function returns the maximum of the distance values
* @param    int index
*/
float LaserSensor::getMax(int& index) {
	float max;
	max = ranges[0];
	index = 0;
	for (int i = 0; i < 181; i++)
	{
		if (ranges[i] > max)
		{
			max = ranges[i];
			index = i;
		}
	}
	return max;
}
/**
* This function returns the sensor value given the index.
* @param    int i
*/
float LaserSensor::operator[](int i) {
	return this->ranges[i];
}
/**
* This function returns angle value of the sensor with index.
* @param    int index
*/
float LaserSensor::getAngle(int index) {
	robotAPI = new PioneerRobotAPI;

	float angle, yonelim;
	yonelim = robotAPI->getTh();
	if (index > 90) {
		angle = yonelim + (index - 90);
	}
	else {
		angle = yonelim - (90 - index);
	}
	if (angle < 0)angle = angle + 360;
	return angle;
}
/**
* This function returns the angle of the smallest of the distances between startAngle and endAngle on angle..
* @param    float startAngle
* @param    float endAngle
* @param    float& angle
*/
float LaserSensor::getClosestRange(float startAngle, float endAngle, float& angle) {
	int i = startAngle;
	float minRange = ranges[i];
	for (int i = startAngle; i < endAngle; i++) {
		if (ranges[i] < minRange) {
			minRange = ranges[i];
			angle = i;
		}
	}
	return minRange;
}
/**
* Destructure of LaserSensor
*/
LaserSensor::~LaserSensor()
{
}