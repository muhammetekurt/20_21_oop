#include "RobotControl.h"


RobotControl::RobotControl(RobotInterface* robotInterface, Pose* pos, RangeSensor* sensor,RobotOperator* op)
{
	this->robotInterface = robotInterface;
	this->position = pos;
	this->robotInterface->setPosition(pos);
	this->op=op;
}
void RobotControl::stopTurn()
{
	if (op->getAccessState() == true)
	{
		robotInterface->stopTurn();
	}
	else
	{
		cout << "You must to open access, Please go to access Menu" << endl;
	}
}
void RobotControl::stopMove()
{
	if (op->getAccessState() == true)
	{
		robotInterface->stopMove();
	}
	else
	{
		cout << "You must to open access, Please go to access Menu" << endl;
	}
}



void RobotControl::turnLeft()
{
	if (op->getAccessState() == true)
	{
		robotInterface->turnLeft();
	}
	else
	{
		cout << "You must to open access, Please go to access Menu" << endl;
	}
}

void RobotControl::turnRight()
{
	if (op->getAccessState() == true)
	{
		robotInterface->turnRight();
	}
	else
	{
		cout << "You must to open access, Please go to access Menu" << endl;
	}
}

void RobotControl::forward(float speed)
{
	if (op->getAccessState() == true)
	{
		robotInterface->stopMove();

		robotInterface->move(speed);
	}
	else
	{
		cout << "You must to open access, Please go to access Menu" << endl;
	}
}
void RobotControl::backward(float speed)
{
	if (op->getAccessState() == true)
	{
		robotInterface->stopMove();

		robotInterface->move(-speed);
	}
	else
	{
		cout << "You must to open access, Please go to access Menu" << endl;
	}
}

void RobotControl::print()
{
	if (op->getAccessState() == true)
	{
		cout << "MyPose is (" << position->getX() << "," << position->getY() << "," << position->getTh() << ")" << endl;
	}
	else
	{
		cout << "You must to open access, Please go to access Menu" << endl;
	}
}

//void RobotControl::moveDistance(float distance)
//{
//	robotInterface->move(distance);
//	Sleep(1000);
//	robotInterface->stop();
//}
void RobotControl::AddtoPath() {
	if (op->getAccessState() == true)
	{
		path->addPos(*position);
	}
	else
	{
		cout << "You must to open access, Please go to access Menu" << endl;
	}
}
void RobotControl::CleartoPath()
{
	if (op->getAccessState()==true)
	{
		path->CleartoPath();
	}
	else
	{
		cout << "You must to open access, Please go to access Menu" << endl;
	}
}
bool RobotControl::openAccess(int kod) 
{
	if (op->checkAccessCode(kod)==true)
	{
		op->setAccessState(true);
		return true;
	}
	else
	{
		op->setAccessState(false);
		return false;
	}
}
bool RobotControl::closeAccess(int kod)
{
	if (op->checkAccessCode(kod)==true)
	{
		op->setAccessState(false);
		return true;
	}
	else
	{
		op->setAccessState(true);
		return false;
	}
}
RobotControl::~RobotControl()
{
}