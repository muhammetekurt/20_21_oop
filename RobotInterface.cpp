#include "RobotInterface.h"


/**
* \brief RobotInterface default constructor
*/
RobotInterface::RobotInterface()
{}
/**
* \brief Set the position info using position class(used aggregation)
* \param Range Sensor info
*/
void RobotInterface::setPosition(Pose* _position)
{
	this->position = _position;
}
/**
* \brief Set the Range Sensor info using RangeSensor class(used aggregation)
* \param Range Sensor info
*/
void RobotInterface::setSensor(RangeSensor* sensor)
{
	this->rangeSensor = sensor;
}
/**
* \brief RobotInterface Destructor
*/
RobotInterface::~RobotInterface()
{
}
