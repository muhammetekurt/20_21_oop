#include "SonarSensor.h"


SonarSensor::SonarSensor()
{
	
}

float SonarSensor::getRange(int index)
{
	return this->ranges[index];
}


float SonarSensor::getMax(int& index)
{
	if (index >= 16)
	{
		throw std::exception("Index is out of range!");
		//return;
	}
	else
	{
		float max = this->ranges[0];
		for (int i = 1; i < 16; i++)
		{
			if (this->ranges[i] > max)
			{
				max = this->ranges[i];
				index = i;
			}
		}
		return max;
	}
}

float SonarSensor::getMin(int& index)
{
	if (index >= 16)
	{
		throw std::exception("Index is out of range!");
		//return;
	}
	else
	{
		float min = this->ranges[0];
		for (int i = 1; i < 16; i++)
		{
			if (this->ranges[i] < min)
			{
				min = this->ranges[i];
				index = i;
			}
		}
		return min;
	}

}

float SonarSensor::getAngle(int index)
{
	if (index >= 16)
	{
		throw std::exception("Index is out of range!");
		//return;
	}
	else
	{
		float angle;
		if (index == 0 || index == 15)
			angle = 90;
		else if (index >= 1 && index <= 6)
		{
			angle = 90 - ((index - 1) * 20 + 40);
		}
		else if (index == 8 || index == 7)
			angle = -90;
		else if (index >= 9 && index <= 14)
		{
			angle = 90 - ((index - 8) * 20 + 40);
		}

		return angle;
	}

}

void SonarSensor::updateSensor(float ranges[])
{
	for (int i = 0; i < 16; i++)
	{
		this->ranges[i] = ranges[i];
	}
}

float SonarSensor::operator[](int i)
{
	if (i >= 16)
	{
		throw std::exception("Index is out of range!");
		//return;
	}
	else
		return this->ranges[i];
}

SonarSensor::~SonarSensor()
{
}


