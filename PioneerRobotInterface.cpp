#include "PioneerRobotInterface.h"
PioneerRobotInterface::PioneerRobotInterface()
{
	setRobot(this);
}

PioneerRobotInterface::PioneerRobotInterface(RangeSensor* sensor, Pose* pos)
{
	setRobot(this);
	this->rangeSensor = sensor;
	this->position = pos;
}


void PioneerRobotInterface::update()
{
	position->setX(getX());
	position->setY(getY());
	position->setTh(getTh());
	setPosition(position);
	float ranges[16];
	float min, max;
	getSonarRange(ranges);
	rangeSensor->updateSensor(ranges);
	setSensor(rangeSensor);
}

bool PioneerRobotInterface::open()
{
	bool status = connect();
	if (status == 1)
		update();

	return status;

}

void PioneerRobotInterface::move(float speed)
{
	moveRobot(speed);
}

bool PioneerRobotInterface::close()
{
	return disconnect();
}

void PioneerRobotInterface::turnLeft()
{
	turnRobot(PioneerRobotAPI::DIRECTION::left);
}
void PioneerRobotInterface::turnRight()
{
	turnRobot(PioneerRobotAPI::DIRECTION::right);
}
void PioneerRobotInterface::Forward(float speed)
{
	move(speed);
}
void PioneerRobotInterface::backward(float speed)
{
	move(-speed);
}

void PioneerRobotInterface::stopTurn()
{
	stopRobot();
}

void PioneerRobotInterface::stopMove()
{
	stopRobot();
}
void PioneerRobotInterface::updateRobot()
{
	position->setX(getX());
	position->setY(getY());
	position->setTh(getTh());
	setPosition(position);
	float ranges[16];
	float min, max;
	getSonarRange(ranges);
	rangeSensor->updateSensor(ranges);
	setSensor(rangeSensor);
}


PioneerRobotInterface::~PioneerRobotInterface()
{

}