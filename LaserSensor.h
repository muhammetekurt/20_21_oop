﻿#pragma once
#pragma once
#include "RangeSensor.h"
using namespace std;
/**
* @file     : LaserSensor.h
* @author   : Yasin Binler - 152120181008
* @date     : 24 January 2021 Monday
* @brief    : This class provides data capture and management for laser distance sensor
* @version  : v1.0
*/
class LaserSensor : public RangeSensor
{
	float ranges[1000];
	PioneerRobotAPI* robotAPI;
public:
	LaserSensor();
	float getRange(int index);
	void updateSensor(float ranges[]);
	float getMax(int& index);
	float getMin(int& index);
	float operator[](int i);
	float getAngle(int index);
	float getClosestRange(float startAngle, float endAngle, float& angle);
	~LaserSensor();
};