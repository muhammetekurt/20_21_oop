#include "Encryption.h"

#include <iostream>
using namespace std;
/*!
\param   code an integer argument.
\return  returns the encrypted version of the code variable.
*/
int Encryption:: encrypt(int code)
{

	int birler = code % 10;//4
	int onlar = ((code % 100)- birler) / 10;//3
	int y�zler = ((code % 1000) - onlar) / 100;//2
	int binler = code / 1000;//1
	
	birler = (birler + 7) % 10;
	onlar = (onlar + 7) % 10;
	y�zler = (y�zler + 7) % 10;
	binler = (binler + 7) % 10;

	return (onlar * 1000 + birler * 100 + binler * 10 + y�zler); 
}

/*!
\param   code an integer argument.
\return  returns the decrypted version of the code variable.
*/
int Encryption::decrypt(int code)
{
	int birler = code % 10;//4
	int onlar = ((code % 100) - birler) / 10;//3
	int y�zler = ((code % 1000) - onlar) / 100;//2
	int binler = code / 1000;//1
	
	birler = (birler + 3) % 10;
	onlar = (onlar + 3) % 10;
	y�zler = (y�zler + 3) % 10;
	binler = (binler + 3) % 10;

	return (onlar * 1000 + birler * 100 + binler * 10 + y�zler);  
}
