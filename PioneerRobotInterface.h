#pragma once
#include "RobotInterface.h"
#include "PioneerRobotAPI.h"
#include "SonarSensor.h"
#include "Pose.h"

/**
* \@File PioneerRobotInterface.h
* \@Author Halitcan Turan 152120171108
* \@Author S�leyman Kanal 152120181007
* \@Date
*
* \brief PioneerRobotInterface class.
* \brief This class, using the functions of the Pioneer robot simulator,our design provides the interface.
* \brief Pure abstract functions in RobotInterface class,using the functions of the PioneerRobotAPI class.
*/
class PioneerRobotInterface : public RobotInterface, public PioneerRobotAPI
{
	/**
	* \brief range sensor attribute
	*/
	RangeSensor* rangeSensor;
	/**
	* \brief position attribute
	*/
	Pose* position;
public:
	/**
	* \brief Default PioneerRobot constructor
	*/
	PioneerRobotInterface();
	/**
	* \brief PioneerRobotInterface constructor
	* \param sensor 
	* \param position 
	*/
	PioneerRobotInterface(RangeSensor* sensor, Pose* pos);
	/**
	* \brief This function is used to update location and sensor information.
	*/
	void update();
	/**
	* \brief This function opens the link with the robot simulator and makes the robota accessible.
	*/
	bool open();
	/**
	* \brief The progress of the robot is provided, at the speed given as parameter
	* \brief The robot goes backwards in "-" values.
	* \param speed information.
	*/
	void move(float speed);
	/**
	* \brief This function closes the link with the robot simulator
	*/
	bool close();
	/**
	* \brief This function turns left.
	*/
	void turnLeft();
	/**
	* \brief This function turns right.
	*/
	void turnRight();
	/**
	* \brief This function turns forward.
	*/
	void Forward(float);

	/**
	* \brief This function turns backward.
	*/
	void backward(float);
	/**
	* \brief This function stops turn
	*/
	void stopTurn();
	/**
	* \brief This function is used to  update robot's location and sensor information.
	*/
	void updateRobot();
	/**
	* \brief PioneerRobotInterface destructor
	*/
	~PioneerRobotInterface();
	/**
	* \brief This function stops move.
	*/
	void stopMove();

};