#pragma once
#include <vector>
#include <math.h>
#include "RobotInterface.h"
#include "RobotOperator.h"
#include "RangeSensor.h"
#include "SonarSensor.h"
#include "Pose.h"
#include "PioneerRobotInterface.h"
#include "Path.h"

using namespace std;

/**
 * @brief Robot hareketlerini yoneten sinif
 *
 */
class RobotControl
{
	Path* path;
	Pose* position;
	RobotInterface* robotInterface;
	RobotOperator* op;
public:
	RobotControl();
	RobotControl(RobotInterface* robotInterface, Pose* pos, RangeSensor* sensor, RobotOperator* op);
	/**
	 * @brief Robotu sola dogru dondurmeye baslayan fonksiyon.
	 */
	void turnLeft();
	/**
	 * @brief Robotu sa�a dogru dondurmeye baslayan fonksiyon.
	 */
	void turnRight();
	/**
	 * @brief Robotu ileri dogru dondurmeye baslayan fonksiyon.
	 */
	void forward(float);
	/**
	 * @brief Robotu geriye dogru dondurmeye baslayan fonksiyon.
	 */
	void backward(float);

	/**
	* @brief Robotun varsa hareketi durduran fonksiyon.
	*/
	void stopMove();
	/**
	* @brief Robotun varsa donmesini durduran fonksiyon.
	*/
	void stopTurn();

	/**
	* @brief Robotun konum bilgilerini yazd�ran fonksiyon.
	*/
	void print();
	/**
	* @brief Robotun g�ncel konumunu path'in sonuna ekleyen fonksiyon
	*/
	void AddtoPath();
	/**
	* @brief  Path'i tamamen temizleyen fonksiyon
	*/
	void CleartoPath();
	/**
	* @brief  Operatorun robot control s�n�f�ndaki fonksiyonlar� kullanmas�n� sa�lar
	*/
	bool openAccess(int);
	/**
	* @brief  Operatorun robot control s�n�f�ndaki fonksiyonlar� kullanmamas�n� sa�lar
	*/
	bool closeAccess(int);
	/**
	* Destructure of the RobotControl
	*/
	~RobotControl();
};


