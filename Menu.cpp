﻿#include "Menu.h"
#include "SonarSensor.h"
#include "LaserSensor.h"

Menu::Menu()
{
	this->position = new Pose();
	this->sensor = new SonarSensor();
	this->robotInterface = new PioneerRobotInterface(this->sensor, this->position);
	this->op = new RobotOperator(1000);
	this->robotControl = new RobotControl(robotInterface, this->position,this->sensor,this->op);
	this->connectionStatus = false;
}

int  Menu::controlInput(string input)
{
	if (input.length() > 1)
	{
		throw std::exception("Error , Invalid Input!!! ");
	}
	else if (isdigit(input[0]))
	{
		int choice = stoi(input.c_str());
		if (choice > 0 )
		{
			return choice;
		}
		else
		{
			return -1;
		}
	}
	return 0;
}

void Menu::mainMenu()
{
	for(;;)
	{
		string input;
		cout << " ------------------ MENU------------------" << endl;
		cout << " 1. Connection " << endl << " 2. Motion " << endl << " 3. Sensor " << endl <<" 4. Access "<<endl<< " 5. Quit " << endl;
		cout << " Choose one : ";
		cin >> input;

		cout << input;
		int choice = controlInput(input);		
		switch (choice)
		{
		case 1:
			system("cls");
			connectionMenu();
			break;
		case 2:
			system("cls");
			motionMenu();
			break;
		case 3:
			system("cls");
			sensorMenu();
			break;
		case 4:
			system("cls");
			AccessMenu();
			break;
		case 5:
			cout << "Exitting the program " << endl;
			Sleep(200);
			return;
		default:
			cout << "Please enter input between 1 and 5..." << endl;
			system("cls");
			break;
		}
	}
}

void Menu::connectionMenu()
{
	while (true)
	{
		string input;
		cout << " ------------------ Connection Menu------------------" << endl;
		cout << "1. Connect Robot" << endl << "2. Disconnect Robot" << endl << "3. Back" << endl;
		cout << "Choose one : ";
		cin >> input;
		int choosen = controlInput(input);
		switch (choosen)
		{
		case 1:
			if (!connectionStatus)
			{
				if (!robotInterface->open()) {
					cout << "Could not connect..." << endl;
					cout << "Please open mobile sim if mobile sim opened that means unknown error" << endl;
					Sleep(200);
					system("cls");
					return;
				}
				else
				{
					cout << "Connected to the robot..." << endl;
					Sleep(200);
					system("cls");
					connectionStatus = true;
				}
			}
			else
			{
				cout << "Robot Connection Already Established" << endl;
				Sleep(200);
				system("cls");
			}
			break;
		case 2:
			if (connectionStatus)
			{
				if (!robotInterface->close())
				{
					cout << "Robot connection could not closed.Seems have Mobile Sim has a problem" << endl;
					Sleep(200);
					system("cls");
				}
				else
				{
					cout << "Disconnected to the robot" << endl;
					connectionStatus = false;
					Sleep(200);
					system("cls");
				}
			}
			else
			{
				cout << "Robot connection already closed" << endl;
				Sleep(200);
				system("cls");
			}
			break;
		case 3:
			system("cls");
			return;
		default:
			cout << "Please enter input between 1 and 3" << endl;
			Sleep(200);
			system("cls");
			break;
		}
	}
}

void Menu::motionMenu()
{
	for (;;)
	{
		if (connectionStatus)
		{
			string input;
			cout << " ------------------Motion Menu------------------" << endl;
			cout << "1. Move Robot" << endl << "2. Turn Left" << endl << "3. Turn Right" << endl
				<< "4. Forward" << endl << "5. Backward" << endl << "6. Stop Robot" << endl<<"7. Stop Turn"<<endl
				<< "8. Print Data" << endl << "9. Back" << endl;
			cin >> input;
			int choosen;
			choosen= controlInput(input);
			float speedFloat = 0.0;
			switch (choosen)
			{
			case 1:
				speedFloat = askSpeed();
				if ((int)speedFloat == 0)
				{
					cout << "You have entered wrong input don't enter 0 or char value" << endl;
					system("cls");
					Sleep(1000);
				}
				else
				{
					robotInterface->move(speedFloat);
					system("cls");
				}
				break;
			case 2:
				robotControl->turnLeft();
				if (op->getAccessState() == true)
				{
					cout << "Turning Left" << endl;
				}
				Sleep(1000);
				system("cls");
				break;
			case 3:
				robotControl->turnRight();
				if (op->getAccessState()==true)
				{
					cout << "Turning Right" << endl;
				}
				Sleep(1000);
				system("cls");
				break;
			case 4:
				speedFloat = askSpeed();
				robotControl->forward(speedFloat);
				if (op->getAccessState() == true)
				{
					cout << "Moving Forward" << endl;
				}
				Sleep(2000);
				system("cls");
				break;
			case 5:
				speedFloat = askSpeed();
				robotControl->backward(speedFloat);
				if (op->getAccessState() == true)
				{
					cout << "Moving Backward" << endl;
				}
				Sleep(2000);
				system("cls");
				break;
			case 6:
				robotControl->stopMove();
				if (op->getAccessState() == true)
				{
					cout << "Robot stopped to move.." << endl;
				}
				Sleep(2000);
				system("cls");
				break;
			case 7:
				robotControl->stopTurn();
				if (op->getAccessState() == true)
				{
					cout << "Robot stopped to turn.." << endl;
				}
				Sleep(2000);
				system("cls");
				break;
			case 8:
				robotControl->print();
				Sleep(2000);
				system("cls");
				break;
			case 9:
				system("cls");
				return;
			default:
				cout << "Please enter input between 1 and 9..." << endl;
				Sleep(2000);
				system("cls");
				break;
			}
		}
		else
		{
			system("cls");
			cout << "Robot connection is closed, Please open connection  at first" << endl;
			Sleep(1000);
			system("cls");
			return;
		}
	}
}

void Menu::sensorMenu()
{
	if (connectionStatus)
	{
		for (;;)
		{
			string input;
			cout << " ------------------Sensor Menu------------------" << endl;
			cout << "1. Get Min" << endl << "2. Get Max" << endl << "3. Get Range" << endl << "4. Get Angle" << endl << "5. Back" << endl;
			cout << "Choose one : ";
			cin >> input;
			int choice = controlInput(input);
			int range = 0;
			int angle = 0;
			switch (choice)
			{
			case 1:
				int index;
				cout << sensor->getMin(index) << endl;
				Sleep(1000);
				system("cls");
				break;
			case 2:
				cout << sensor->getMax(index) << endl;
				Sleep(1000);
				system("cls");
				break;
			case 3:
				range = askAngle();
				cout << sensor->getRange(range) << endl;
				Sleep(1000);
				system("cls");
				break;
			case 4:
				index = askAngle();
				cout << sensor->getAngle(index) << endl;
				Sleep(1000);
				system("cls");
				break;
			case 5:
				system("cls");
				return;
			default:
				cout << "Please enter input between 1 and 5" << endl;
				Sleep(1000);
				system("cls");
				break;
			}
		}
	}
	else
	{
		system("cls");
		cout << "Robot connection closed please open connection at first" << endl;
		Sleep(1000);
		system("cls");
		return;
	}
}
void Menu::AccessMenu() {
	for (;;)
	{
		string input;
		cout << " ------------------Access Menu------------------" << endl;
		cout << "1. Open Access" << endl << "2. Close Access" << endl << "3. Back" << endl;
		cin >> input;
		int choice = controlInput(input);
		switch (choice)
		{
		case 1:
			int code;
			cout << "Enter a code :" << endl;
			cin >> code;
			if (robotControl->openAccess(code) == true)
			{
				cout << "Opened to access..." << endl;
			}
			else
			{
				cout << "The code was wrong, Please try again ...." << endl;
			}
			Sleep(2000);
			system("cls");
			break;
		case 2:
			cout << "Enter a code :" << endl;
			cin >> code;
			if (robotControl->closeAccess(code) == true)
			{
				cout << "Closed to access..." << endl;
			}
			else
			{
				cout << "The code was wrong, Please try again ...." << endl;
			}
			Sleep(2000);
			system("cls");
			break;
		case 3:
			system("cls");
			return;
		default:
			cout << "Please enter input between 1 and 3" << endl;
			Sleep(2000);
			system("cls");
			break;
		}
	}
}
int Menu::askAngle()
{
	string input;
	int angle;
	cout << "Enter index of sensor :" << endl;
	cin >> input;
	angle = controlInteger(input);
	return angle;
}

float Menu::askSpeed()
{
	string input;
	float speed;
	cout << "Enter speed :" << endl;
	cin >> input;
	speed = controlFloat(input);
	return speed;
}

float Menu::controlFloat(string input)
{
	float value;
	try
	{
		value = stof(input);
	}
	catch (const std::exception&)
	{
		return 0;
	}
	return value;
}

int Menu::controlInteger(string input)
{
	int value;
	try
	{
		value = stoi(input);
	}
	catch (const std::exception&)
	{
		return 0;
	}
	return value;
}




Menu::~Menu()
{
}