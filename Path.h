/**
* @file     : Path.h
* @author   : Yasin Binler - 152120181008
* @date     : 24 January 2021 Monday
* @brief    : This class provides data capture and management for laser distance sensor
* @version  : v1.0
*/

#ifndef PATH_H_
#define PATH_H_
#include"Node.h"
#include "Pose.h"

class Path{
private:
	 Node* head;
	 Node* tail;
	 int number;
public:

	void addPos(Pose pose);
	void print();
	Pose operator[](int i);
	Pose getPos(int index);
	void removePos(int index);
	void insertPose(int index, Pose pose);
	void CleartoPath();
};
#endif // !1
