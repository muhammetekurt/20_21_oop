
/**
* @file     : Encryption.h
* @author   : Halitcan Turan - 152120171108
* @date     : 9 January 2021 Saturday
* @brief    : This class provides encryption and decryption of 4-digit numbers.
* @version  : v1.0
*/

class Encryption
{
public:
	int encrypt(int);//A sample function
	int decrypt(int);//A sample function
};

