#include"Menu.h"

int main() {
	try {
		Menu menu;
		menu.mainMenu();
	}
	catch (exception const& ex) {
		cerr << "Exception: " << ex.what() << endl;
	}

	return 0;
}