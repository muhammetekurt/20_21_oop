#pragma once
/**
 * @brief Pozisyon bilgilerini tutan sinif
 *
 */
class Pose
{
	float x;
	float y;
	float th;
public:
	Pose();
	/**
	 * @brief X pozisyonu icin getter fonksiyonu.
	 *
	 * @return float - Tanimli koordinat.
	 */
	float getX();
	/**
	 * @brief X pozisyonu icin setter fonksiyonu.
	 *
	 * @param x - Atanacak koordinat.
	 */
	void setX(float x);
	/**
	 * @brief Y pozisyonu icin getter fonksiyonu.
	 *
	 * @return float - Tanimli koordinat.
	 */
	float getY();
	/**
	 * @brief Y pozisyonu icin setter fonksiyonu.
	 *
	 * @param y - Atanacak koordinat.
	 */
	void setY(float y);
	/**
	 * @brief Th pozisyonu icin getter fonksiyonu.
	 *
	 * @return float - Tanimli koordinat.
	 */
	float getTh();
	/**
	 * @brief Th pozisyonu icin setter fonksiyonu.
	 *
	 * @param Th - Atanacak koordinat.
	 */
	void setTh(float th);
	/**
	 * @brief Pozisyonlar arasi karsilastirma icin operator overloading.
	 * @param rhs - Karsilastirilacak pozisyon degeri
	 * @return true - Tum degerlerin esit olmasi durumunda dondurulur.
	 * @return false - Tum degerler esit degilse dondurulur.
	 */
	bool operator==(const Pose& pos);
	/**
	 * @brief Pose poss pozisyonu icin setter fonksiyonu.
	 *
	 * @param poss - Atanacak koordinat.
	 */
	void setPose(Pose poss);

	~Pose();
};
