#pragma once
#include "Pose.h"
#include "RangeSensor.h"
/**
* \@File RobotInterface.h
* \@Author Muhammed Emin Kurt - 152120171045
* \@Date 24 January 2021 Monday
*
* \brief RobotInterface
* \brief This class is abstract class.
* \brief This class is Interface task for design simulator access and now It provides the interface to the Pioneer simulator.
* \brief But In the future, classes such as the PioneerRobotInterface class for different robot simulators will be added to
* \brief provide an interface within them.
*/
class RobotInterface
{
	/**
	* \brief position attribute(from Position class using aggregation)
	*/
	Pose* position;
	/**
	* \brief range sensor attribute(from RangeSensor class using aggregation)
	*/
	RangeSensor* rangeSensor;
public:

	//!An enum DIRECTION
	/*! This enum is used for robot directions */
	enum DIRECTION
	{
		left = -1,     
		forward = 0,   
		right = 1 
	};
	/**
	* \brief RobotInterface default constructor
	*/
	RobotInterface();
	/**
	* \brief Setter function of position pointer information
	* \param position 
	*/
	void setPosition(Pose* pos);
	/**
	* \brief Setter function of Range sensor pointer information
	* \param Range Sensor* sensor
	*/
	void setSensor(RangeSensor* sensor);
	/**
	* \brief Pure virtual function set sensor
	* \brief This function definition is given derivated class from this class
	*/
	virtual void update() = 0;
	/**
	* \brief Pure virtual function open (used polymorphism)
	* \brief This function definition is given derivated class from this class
	*/
	virtual bool open() = 0;
	/**
	* \brief Pure virtual function open(used polymorphism)
	* \brief This function definition is given derivated class from this class
	*/
	virtual bool close() = 0;
	/**
	* \brief Pure virtual function move
	* \brief This function definition is given derivated class from this class
	*/
	virtual void move(float speed) = 0;
	/**
	* \brief Pure virtual function turnLeft
	* \brief This function definition is given derivated class from this class
	*/
	virtual void turnLeft() = 0;
	/**
	* \brief Pure virtual function turnRight
	* \brief This function definition is given derivated class from this class
	*/
	virtual void turnRight() = 0;
	/**
	* \brief Pure virtual function Forward
	* \brief This function definition is given derivated class from this class
	*/
	virtual void Forward(float) = 0;
	/**
	* \brief Pure virtual function Backward
	* \brief This function definition is given derivated class from this class
	*/
	virtual void backward(float) = 0;

	/**
	* \brief Pure virtual function stopMove
	* \brief This function definition is given derivated class from this class
	*/
	virtual void stopMove() = 0;
	/**
	* \brief Pure virtual function stopTurn
	* \brief This function definition is given derivated class from this class
	*/
	virtual void stopTurn() = 0;


	/**
	* \brief RobotInterface Destructor
	*/
	~RobotInterface();

};