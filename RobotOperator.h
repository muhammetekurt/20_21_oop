
/**
* @file     : RobotOperator.h
* @author   : Halitcan Turan - 152120171108
* @date     : 24 January 2021 Monday
* @brief    : This class authorizes the operator to command the robot in robot applications.
* @version  : v1.0
*/

#pragma once
#include<iostream>

using namespace std;

class RobotOperator
{
private:
	string name;
	string surname;
	unsigned int accessCode;
	bool accessState;
	int EncryptCode(int);
	int DecryptCode(int);
public:
	RobotOperator(unsigned int);
	RobotOperator(string name, string surname, unsigned int accessCode);
	void setAccessState(bool state);
	bool getAccessState();
	bool checkAccessCode(int);
	void print();
};
