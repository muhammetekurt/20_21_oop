#include "Pose.h"

Pose::Pose()
{
	this->x = 0;
	this->y = 0;
	this->th = 0;
}

float Pose::getX()
{
	return this->x;
}

void Pose::setX(float x)
{
	this->x = x;
}

float Pose::getY()
{
	return this->y;
}

void Pose::setY(float y)
{
	this->y = y;
}

float Pose::getTh()
{
	return this->th;
}

void Pose::setTh(float th)
{
	this->th = th;
}

void Pose::setPose(Pose poss) {
	poss.setX(poss.getX());
	poss.setY(poss.getY());
	poss.setTh(poss.getTh());
}

bool Pose::operator==(const Pose& pos)
{
	if (this->getX() != pos.x)
		return false;
	if (this->getY() != pos.y)
		return false;
	if (this->getTh() != pos.th)
		return false;
	return true;
}


Pose::~Pose()
{
}