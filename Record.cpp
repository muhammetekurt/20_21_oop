#include "Record.h"

bool Record::openFile()
{
	if (fileName.length() != 0)
	{
		this->file.open(this->fileName, std::fstream::in | std::fstream::out | std::fstream::trunc);
		if (this->file.is_open())
		{
			return true;
		}
		else
		{
			throw("File could not be opened");
			return false;
		}
	}
	else
	{
		throw("Please set a file name");
		return false;
	}
}

bool Record::closeFile()
{
	if (this->file.is_open())
	{
		this->file.close();
	}
	else
	{
		throw("File is not open");
	}

	return !this->file.is_open();
}

void Record::setFileName(string name)
{
	this->fileName = name;
}

string Record::readLine()
{
	if (this->fileName.length() > 0)
	{
		if (this->file.is_open())
		{
			string s;
			getline(this->file, s);
			return s;
		}
		else
		{
			throw("File is not open");
			//return;
		}
	}
	else
	{
		throw("Please set a file name");
		//return;
	}
		
}

bool Record::writeLine(string str)
{
	if (this->fileName.length() > 0)
	{
		if (this->file.is_open())
		{
			this->file << str;
			return true;
		}
		else
		{
			throw("File is not open");
			return false;
		}
	}
	else
	{
		throw("Please set a file name");
		return false;
	}
}

