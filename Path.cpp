#include "Path.h"
#include"Pose.h"
#include<iostream>
#include<iomanip>

using namespace std;
/**
* This function adds the given pos location to the end of the list.
* @param    Pose pose
*/

void Path::addPos(Pose pose) {
    Node* tmp = new Node;
    tmp->pose = pose;
    tmp->next = NULL;
    tmp->index = number;

    if (head == NULL)
    {
        head = tmp;
        tail = tmp;
        number++;
    }
    else
    {
        tail->next = tmp;
        tail = tail->next;
        number++;
    }
}
/**
* This function prints the locations in the list on the screen.
*/
void Path::print() {
    cout << "-----------POSITIONS-----------" << endl;
    Node* temp = head;
    while (temp!= NULL) {
        cout << "   X:" << setw(3) << temp->pose.getX() << "   Y:" << setw(3) << temp->pose.getY() << "    TH:" << setw(3) << temp->pose.getTh() << endl;
        temp = temp->next;
    }
    delete temp;
}
Pose Path::operator[](int i) {
    Node* temp = head;
    while (temp != NULL) {
        if (temp->index == i) {
            return temp->pose;
        }
        temp = temp->next;
    }
    delete temp;
}
/**
* This function returns the position in the given index
* @param    int index
*/
Pose Path::getPos(int index) {
    Node* temp = head;
    while (temp != NULL) {
        if (temp->index == index) {
            return temp->pose;
        }
        temp = temp->next;
    }
    delete temp;
}
/**
* This function delete the position in the given index
* @param    int index
*/
void Path::removePos(int index) {

    Node* temp = head;
    Node* prev = NULL;

   
    if (temp != NULL && temp->index == index)
    {
        head = temp->next;
        delete temp;            
        return;
    }

    
    while (temp != NULL && temp->index != index)
    {
        prev = temp;
        temp = temp->next;
    }

    if (temp == NULL)
        return;

    prev->next = temp->next;

    delete temp;
    number--;
}
/**
* This function adds a position after the given index.
* @param    int index
* @param    Pose pose
*/
void Path::insertPose(int index, Pose pose) {
    Node* tmp = new Node;
    Node* temp = head;
    tmp->pose = pose;
    tmp->next = NULL;
    while (temp != NULL) {
        if (temp->index == index) {
            tmp->next = temp->next;
            temp->next = tmp;
            break;
        }
        temp = temp->next;
    }
}
/**
* This function clear all  position on the path .
*/
void Path::CleartoPath() 
{
    Node** head_ref=NULL;
    *head_ref = this->head;
    Node * current = *head_ref;
    Node* next;

    while (current != NULL)
    {
        next = current->next;
        free(current);
        current = next;
    }

    *head_ref = NULL;
}