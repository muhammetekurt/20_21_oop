#pragma once
#include "Pose.h"
#include <string>
#include <fstream>
using namespace std;

/**
* @file Record.h
* @Author S�leyman Kanal
* @date January, 2021
* @brief This file manages the file input and output operations.
*/


//! A record class
/*! 
	The open file function opens file by using open function in the fstream library.
	The close file function closes file by using close function in the fstream library.
	The setFileName function takes a string as a prameter and changes the file name with this parameter.
	The readLine function reads a line from the file and returns it.
	The writeLine function takes a string as a parameter a writes it to the file returns the result as true or false value.
	The operator>> function overloads << to read a file.
*/
class Record
{

private:
	string fileName;
	fstream file;
	Pose *p = NULL;


public:
	bool openFile();
	bool closeFile();
	void setFileName(string name);
	string readLine();
	bool writeLine(string str);
	friend ostream& operator<<(ostream& os, const Record& rc);
	friend ostream& operator>>(ostream& os, const Record& rc);




};

ostream& operator<<(ostream& os, const Record& rc)
{
	while (!rc.file.eof())
	{
		os << "The x is " << rc.p->getX() << " The y is " << rc.p->getY() << "The th is " << rc.p->getTh();

		return os;
		
	}
}

//ostream& operator>>(ostream& os, const Record& rc)
//{
//	//os << rc.writeLine(std::to_string(rc.p->getX()));
//
//}