#pragma once
#include "RangeSensor.h"
#include "PioneerRobotAPI.h"
/**
* @file SonarSensor.h
* @Author S�leyman Kanal
* @date January, 2021
* @brief This file takes information about sonar sensor
*/


//! A sonar sensor class
/*!
	The getRange function takes an integer as a parameter and returns the distance information at that index.
	The getMax function returns the maximum value from the ranges array.
	The getMin function returns the minimum value from the ranges array.
	The updateSensor function stores the values in ranges array came from robotAPI object.
	The getAngle function takes an integer as a parameter and returns the angle of the sensor at that index as a float value.
	The operator[] function overloads [] to take an integer that will be the index of the sensor and returns the distance information at that index.
*/
class SonarSensor : public RangeSensor
{
	float ranges[16];
	PioneerRobotAPI* robotAPI;
public:
	SonarSensor();
	float getRange(int index);
	float getMax(int& index);
	float getMin(int& index);
	void updateSensor(float ranges[]);
	float operator[](int i);
	float getAngle(int index);
	~SonarSensor();
};

