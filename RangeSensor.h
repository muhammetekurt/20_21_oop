﻿#pragma once
#include <string>
#include "PioneerRobotAPI.h"
using namespace std;
/**
* @file     : RangeSensor.h
* @author   : Yasin Binler - 152120181008
* @date     : 24 January 2021 Monday
* @brief    : This class is abstract.
* @version  : v1.0
*/
class RangeSensor
{
	float ranges[16];
	PioneerRobotAPI* robotAPI;
public:
	/**
	* \ Constructure of the RangeSensor
	*/
	RangeSensor();
	/**
	* \brief Pure virtual function update sensor
	* \brief This function update  sensor is given derivated class from this class
	*/
	virtual void updateSensor(float ranges[]) = 0;
	/**
	* \brief Pure virtual function getMin
	* \brief This function definition is given derivated class from this class
	*/
	virtual float getMin(int &index) = 0;
	/**
	* \brief Pure virtual function getMax
	* \brief This function definition is given derivated class from this class
	*/
	virtual float getMax(int &index) = 0;
	/**
	* \brief Pure virtual function getRange
	* \brief This function definition is given derivated class from this class
	*/
	virtual float getRange(int index) = 0;
	/**
	* \brief Pure virtual function getAngle
	* \brief This function definition is given derivated class from this class
	*/
	virtual float getAngle(int index) = 0;
	/**
	* \brief Pure virtual operator
	* \brief This function definition is given derivated class from this class
	*/
	virtual float operator[](int i) = 0;
	/**
	* \ Destructure of the RangeSensor
	*/
	~RangeSensor();
};


