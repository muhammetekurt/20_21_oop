/**
* @file     : Node.h
* @author   : Yasin Binler - 152120181008
* @date     : 24 January 2021 Monday
* @brief    : This header file will use with Path class.
* @version  : v1.0
*/

#ifndef NODE_H_
#define NODE_H_
#include"Pose.h"

class Node {
public:
	Node* next;
	Pose pose;
	int index;
};
#endif // !NODE_H_