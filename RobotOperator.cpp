#include "RobotOperator.h"
#include"Encryption.h"
#include<string>
/**
* Constructor of the class
*/
RobotOperator::RobotOperator(unsigned int code)
{
	this->accessCode = code;
	this->accessCode = EncryptCode(accessCode);
	bool accessState = false;
}
/**
* Constructor of the class
*/
RobotOperator::RobotOperator(string ad, string soyad, unsigned int Code)
	: name(ad), surname(soyad), accessCode(Code)
{
	this->accessCode = EncryptCode(accessCode);
	bool accessState = false;
}
/**
* This function encrypts the 4 digit code using the function of the Encryption class.
*/
int RobotOperator::EncryptCode(int code) 
{
	Encryption e;
	code=e.encrypt(code);
	return code;
}
/**
* This function decrypts the 4 digit code using the function of the Encryption class.
*/
int RobotOperator::DecryptCode(int code)
{
	Encryption e;
	code=e.decrypt(code);
	return code;
}
/**
* This function check the code entered is the same as the accessCode kept encrypted.
*/
bool RobotOperator::checkAccessCode(int code) 
{
	Encryption e;
	if (code == e.decrypt(this->accessCode))
	{
		return true;
	}
	else
	{
		return false;
	}
}
/**
* This Function sets the accessState
*/
void RobotOperator::setAccessState(bool state)
{
	this->accessState = state;
}
/**
* This Function return the accessState
*/
bool RobotOperator::getAccessState()
{
	return this->accessState;
}
/**
* This function print the operator's informations.
*/
void RobotOperator::print() {

	cout <<"Operator's name :"<< name<<endl;
	cout << "Operator's surname :" << surname<<endl;
	if (accessState==true)
	{
		cout << "Operator's access enabled"  << endl;
	}
	else
		cout << "Operator's access disabled" << endl;
}