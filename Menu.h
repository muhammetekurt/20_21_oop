#pragma once
#include "RobotInterface.h"
#include "RobotControl.h"

#include <iostream>
class Menu
{
private:
	Pose* position;
	RangeSensor* sensor;
	RobotInterface* robotInterface;
	RobotControl* robotControl;
	bool connectionStatus;
	RobotOperator* op;
public:
	Menu();
	int controlInput(string input);
	void mainMenu();
	void connectionMenu();
	void motionMenu();
	void sensorMenu();
	void AccessMenu();
	float askSpeed();
	int askAngle();
	float controlFloat(string input);
	int controlInteger(string input);
	~Menu();
};